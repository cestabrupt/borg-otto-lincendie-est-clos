// Include Gulp
var gulp = require('gulp'),
    uglify = require('gulp-uglify-es').default,
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    postcss = require('gulp-postcss'),
    precss = require('precss'),
    cssImport = require('postcss-import'),
    cache = require('gulp-cache');
    imagemin = require('gulp-imagemin'),
    filter = require('gulp-filter'),
    mqpacker = require('css-mqpacker'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    flexbugs = require('postcss-flexbugs-fixes'),
    babel = require('gulp-babel'),
    del = require('del'),
    merge = require('merge-stream');

// For running a local machine task
var exec = require('child_process').exec;

// CSS
gulp.task('css', function(){
  // del(['gabarit/antilivre.css']);
  var processors = [
    cssImport(),
    precss(),
    mqpacker({
      sort: true
    }),
    // autoprefixer({browsers: ['> 1%', 'last 3 versions', 'firefox >= 4', 'safari >= 7', 'iOS >= 7', 'IE 8', 'IE 9', 'IE 10', 'IE 11'], cascade: false}),
    flexbugs(),
    autoprefixer(),
    cssnano({
      reduceIdents: false,
      normalizeUrl: {
        stripWWW: false
      }
    })
  ];
  return gulp.src('gabarit/src/css/antilivre.css')
    .pipe(postcss(processors))
    .pipe(gulp.dest('gabarit/'));
});


//compress all images
 gulp.task('img', () => {
   const f = filter(['gabarit/src/img/**', '!gabarit/src/img/favicon/*'], { restore: true });
   return gulp.src('gabarit/src/img/**')
        .pipe(f)
        .pipe(cache(imagemin([
        imagemin.gifsicle({interlaced: true}),
        imagemin.jpegtran({progressive: true}),
        imagemin.optipng({optimizationLevel: 5}),
        imagemin.svgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
            ]
        })
        ], {
            verbose: true
        }
        )))
        .pipe(f.restore)
        .pipe(gulp.dest('gabarit/img/'));
});

// Javascript
  // del(['gabarit/antilivre.js']);
gulp.task('javascript', function(){
  var script = gulp.src(['gabarit/src/js/**'])
    .pipe(concat('antilivre.js'))
    // .pipe(uglify())
    .pipe(gulp.dest('gabarit/'));
    // .pipe(reload({stream:true}));
  return merge(script);
});


// Clear cache
gulp.task('clear', () =>
    cache.clearAll()
);

gulp.task(
  'pandoc-simple',
  gulp.series(
  function (cb) {
  exec("make html", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  }
  )
);
gulp.task(
  'pandoc',
  gulp.series(
    'css', 'img', 'javascript',
  function (cb) {
  exec("make html", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  }
  )
);

// Browser-sync
gulp.task('browser-sync', function(cb) {
  browserSync({
    server: {
      baseDir: "public"
    },
    open: false
  }, cb);
});

function reload(done) {
  browserSync.reload();
  done();
}
gulp.task('watch', function () {
  gulp.watch([
    'texte/**',
    'gabarit/*.html',
  ], gulp.series('pandoc-simple', reload));
  gulp.watch([
    'gabarit/src/css/**',
  ], gulp.series('css', 'pandoc-simple', reload));
  gulp.watch([
    'gabarit/src/js/**',
  ], gulp.series('javascript', 'pandoc-simple', reload));
  gulp.watch([
    'gabarit/src/img/**',
  ], gulp.series('img', 'pandoc-simple', reload));
});

gulp.task(
  'default',
  gulp.series('pandoc', 'browser-sync', 'watch')
);

