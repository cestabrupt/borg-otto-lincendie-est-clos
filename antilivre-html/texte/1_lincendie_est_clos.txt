---
author: "Otto Borg"
title: "L'incendie est clos"
publisher: "Abrüpt"
date: "décembre 2019"
lieu: 'Internet & Zürich'
description: ""
subject: "littérature"
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0076-0'
rights: "© 2019 Abrüpt, CC BY-NC-SA"
version: '1.0'
year: 2019
depot: quatrième trimestre 2019
licence: 'Cet ouvrage est mis à disposition selon les termes de la Licence Creative Commons Attribution --- Pas d''Utilisation Commerciale --- Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0).'
lien_licence: 'https://abrupt.ch/partage'
informations_licence: 'Nous avons néanmoins une lecture adaptative de cette licence.'
lien_livre: 'https://abrupt.ch/otto-borg/lincendie-est-clos'
---
