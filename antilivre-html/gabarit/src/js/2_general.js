// Scripts
const fabrique = document.querySelector('.button--shuffle');
const clearBtn = document.querySelector('.button--clear');
const afficheTexte = document.querySelector('.contenu__texte');
// const afficheGraphique = document.querySelector('.contenu__graphique');
const infoBtn = document.querySelector('.button--info');
const paysageBtn = document.querySelector('.button--paysage');
const info = document.querySelector('.informations');
const options = document.querySelector('.options');

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
var content = document.querySelector('.contenu');
content.addEventListener('touchstart', function (event) {
    this.allowUp = (this.scrollTop > 0);
    this.allowDown = (this.scrollTop < this.scrollHeight - this.clientHeight);
    this.slideBeginY = event.pageY;
});

content.addEventListener('touchmove', function (event) {
    var up = (event.pageY > this.slideBeginY);
    var down = (event.pageY < this.slideBeginY);
    this.slideBeginY = event.pageY;
    if ((up && this.allowUp) || (down && this.allowDown)) {
        event.stopPropagation();
    }
    else {
        event.preventDefault();
    }
});

// Shuffle array
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

function Affiche() {

  shuffleArray(incendie);
  let texteFinal = "";

    texteFinal += '<p>';
    texteFinal += incendie[0];
    texteFinal += '</p>';

  afficheTexte.innerHTML = texteFinal;

}


fabrique.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.remove('informations--show');
  Affiche();
});

infoBtn.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.toggle('informations--show');
});


let downloadImage = document.querySelector('.button--img');
downloadImage.addEventListener('click', (e) => {
  e.preventDefault();
  options.classList.add("hide");
  document.documentElement.classList.add("hide-scrollbar");

  html2canvas(document.querySelector('.contenu'),{
    allowTaint: false,
    logging: false,
    backgroundColor: "#000000",
    letterRendering: true,
    scale: 4
  }).then(function(canvas) {
    // const link = document.createElement('a');
    // document.body.appendChild(link);
    // link.download = 'dio.jpg';
    // link.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
    // link.target = '_blank';
    // link.click();
    saveAs(canvas.toDataURL("image/jpeg"), 'lincendie-est-clos.jpg');
  });

  document.documentElement.classList.remove("hide-scrollbar");
  options.classList.remove("hide");
});


function saveAs(uri, filename) {
  const link = document.createElement('a');

  if (typeof link.download === 'string') {
    link.href = uri;
    link.download = filename;
    link.target = '_blank';
    document.body.appendChild(link);
    link.click();
    //remove the link when done
    document.body.removeChild(link);
  } else {
    window.open(uri);
  }
}

// Canvas p5.js


let optionsHeight = document.querySelector('.options').clientHeight;

function setup() {
  const canvas = createCanvas(windowWidth, windowHeight);
  canvas.parent('contenu');
}

function draw() {
  if (mouseIsPressed) {
    if (mouseY > optionsHeight) {
      noStroke();
      fill(255,255,255,50)
      rectMode(CENTER);
      rect(mouseX, mouseY, 70, 70);
    }
  }
}

clearBtn.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.remove('informations--show');
  clear();
});

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

Affiche();


// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});
