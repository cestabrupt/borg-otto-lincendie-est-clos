# ~/ABRÜPT/OTTO BORG/L'INCENDIE EST CLOS/*

La [page de ce livre](https://abrupt.ch/otto-borg/lincendie-est-clos/) sur le réseau.

## Sur le livre

La poésie se fait 99 fois la glaciation en l’urbain, ses campagnes tues. 99 fois la parole qui ne dit pas comment survivre. Mais qui se promène aux alentours de ce qui empêche.

## Sur l'auteur

Post-punk, post-ouvrier, punk quand même, ouvrier encore. Otto Borg est né en Allemagne. Quelque part à l’ombre du mur, sous les nuées de Tchernobyl. Le reste est commun à son siècle. L’usine, tourneur sur métaux. Puis le chômage. Puis une reconversion. Puis menuisier. Un peu musicien aussi. Écrivain, toujours, pour survivre dans l’usine et dans le siècle.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
