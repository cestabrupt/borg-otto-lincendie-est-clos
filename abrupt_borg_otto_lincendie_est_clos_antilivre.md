---
author: "Otto Borg"
title: "L'incendie est clos"
publisher: "ABRÜPT"
date: "juin 2018"
description: ""
subject: ""
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: ''
rights: "© 2018 ABRUPT, CC BY-NC-SA"
---

\p Un homme seul est seul sur l’autoroute. Il décroche sa ceinture. Il accélère.

\p La grève c’est une vacance.

\p Les négociations ont abouti. Des responsables assis face à d’autres responsables. Des sourires, quelques photographies pour les sourires, et les photographies des sourires dans les journaux du soir. Les licenciements seront divisés par deux.

\p Le paysage est vide de villages davantage que de cimetières de village.

\p Une foule. Des centaines de visages. Les mains dans les poches. Une manifestation contre le plan de sauvegarde de l’emploi.

\p Le cours de l’action remonte. Pour que le cours de l’action remonte encore, licencier encore. Le cours de l’action remontera encore.

\p Les délégués du personnel disent qu’il reste si peu à perdre. Le personnel a peur de le perdre.

\p Les rues sont abandonnées par le bruit. Le village ferme ses portes. Dans le champ du repos, le repos des villageois, jusqu’à ce que leur concession prenne fin.

\p Les spectateurs attendent la fin des publicités. Les spectateurs entendent quand même les publicités. Les images indiquent des soldes. Les spectateurs attendent et entendent les images qui indiquent des soldes.

\p Un membre du parti libéral tient un discours dans un bassin minier. Il dit qu’il dit la vérité. Il dit qu’il ne dit pas ce que le bassin minier veut entendre. Les citadins apprécient le membre du parti libéral qui dit qu’il dit la vérité.

\p L’enthousiasme du directeur général de la société est un spectacle. La diffusion de l’enthousiasme du directeur général de la société a été visionnée plusieurs millions de fois sur le réseau dont la société est propriétaire.

\p Les citoyens trouvent que leur choix politique apporte le changement. Ils éprouvent du bonheur. Leur choix politique apporte le changement et le bonheur.

\p Vivre à contretemps du jour, parmi les bureaux et les balais. La poussière accepte de dialoguer en langues étrangères.

\p La ferme a fermé pour cause de pendaison du fermier dans la ferme. Un citadin cherche une alternative à ses vacances à la ferme.

\p Pas sur pavés, la pluie dans les gouttières, quelques lampadaires. Ne pas regarder le haillon qui dort. Peut-être le haillon mort. Ne pas regarder. Rentrer vite pour payer les factures sur l’ordinateur.

\p Dix pièces par heure, trente ans durant. Au bout, trente ans après, des pièces qui sonnent la dissonance. Au bout, trente ans après, plus beaucoup de temps. Au bout, trente ans après, ce qu’il en reste en pièces.

\p Demain, jour chômé. Les suivants aussi.

\p Les taxis sans pilote ne seront plus un lieu où un chauffeur pourra maugréer. Les concitoyens pouvant se payer un taxi s’en réjouissent.

\p Délocalisation confirmée en appel. Rien n’y dit la différence pour la cassation. Doute que décision ne s’y casse.

\p La retransmission d’extraits du discours marque la société. Le discours annonce que les temps vont être durs, mais le discours annonce que la concurrence va entraîner l’investissement. Le discours monte dans les sondages.

\p Dans le palais de justice, le plan social. Au bout de la corde, le bout du plan social.

\p Les syndicats sont divisés quant au projet de loi du gouvernement. Une réunion entre les directions des différents syndicats se tient autour de petits fours et de vins translucides.

\p La campagne n’épargne rien. Un compte courant suffit. L’eau courante au fond de la cour.

\p Dimanche, l’office sur toutes les chaînes. Comme les autres jours, mais c’est dimanche.

\p Un ministre de l’économie s’est suicidé pour une histoire d’adultère avant des pourparlers avec une organisation patronale. La situation sur le marché du travail était pourtant stable malgré les prévisions.

\p Les classes moyennes font le supermarché. Les rabais sont fantastiques.

\p Dix heures trente. Le blanc de dix heures trente. Le deuxième du matin.

\p Un accord ministériel pour sauver l’industrie automobile nationale. La chaîne du travail à la chaîne se raccourcit quelque peu. Le monde s’en félicite.

\p À l’intérieur des jardins ouvriers, la friche. Des jardins. Ouvriers. La friche à l’intérieur. L’engrais est pourtant toujours de bonne qualité dans les jardins ouvriers.

\p Une magasinière a choisi de s’immoler par le feu après que le burnout l’a gagnée. Sa mort a fait grand bruit dans la presse avant que le scandale ne s’éteigne.

\p L’époque apprécie la forme pittoresque des corons. Ces logements deviennent très recherchés sur le marché immobilier.

\p Les livreurs indépendants n’en gardent pas moins une allure parfaitement sympathique.

\p Dans les transports en commun, il arrive que des gens parlent seuls. Une partie d’entre eux placent des oreillettes dans leurs oreilles.

\p La publicité d’un hypermarché pour un crédit à la consommation mettait en évidence un astérisque.

\p S’il est bien porté, un bleu de travail s’avère fort séduisant et très à la mode.

\p Dans le silence de la salle de bain ou dans le garage du pavillon résidentiel, quelques manifestants contre la réforme du droit du travail se sont saisis par le col, seuls par le col à l’aide d’un tuyau dans le silence de la salle de bain ou dans le garage du pavillon résidentiel.

\p Un agent d’entretien nettoie des toilettes. Il a passé sa soirée à nettoyer des toilettes. Rentré au petit matin, son travail à domicile n’est pas son travail : il nettoie des toilettes. Il ne sait pas si l’odeur de détartrant incrustée sur sa peau provient des toilettes ou des toilettes.

\p Compte tenu du nombre de pigeons existant dans l’espace urbain, il paraît étonnant que des sans-abri se plaignent d’avoir faim.

\p Une métallurgiste, au chômage en raison de la fermeture du haut fourneau où elle œuvrait, se rend tous les jours devant le haut fourneau. Elle reste dans sa voiture. Elle y reste quelques heures, elle regarde le haut fourneau. Elle reste le reste chez elle. Elle y reste quelques heures, elle regarde la télévision.

\p Un ouvrier qualifié souhaite être qualifié d’ouvrier qualifié et non de simple ouvrier.

\p Des camions-restaurants proposent pour un prix raisonnable des plats surfins aux salariés à proximité de leur bureau. La cuisine de rue y est raffinée. Les salariés peuvent ainsi rapidement déguster des plats surfins avec leurs mains à proximité de leur bureau.

\p Des grévistes brûlent des pneus devant leur entreprise. Ils hurlent leur mécontentement. Un ministre se déplace avec la presse. Il parle. La presse écoute. Les grévistes écoutent la presse. Les grévistes reprennent le lendemain le travail avec espoir.

\p Un passant aborde un autre passant pour lui dire qu’il a faim. Le passant donne à l’autre passant une pièce.

\p Un couturier a perdu sa main à la suite d’un accident du travail dans la ganterie où il était employé depuis peu. L’entreprise lui a offert pour sa convalescence un de ses produits les plus luxueux.

\p Un employé a été humilié devant tous ses collègues par son patron. En rentrant chez lui, il bat ses enfants.

\p Un cadre supérieur s’est acheté une voiture de sport. Il a demandé un crédit-bail.

\p Il y a une action sur le prix des plats surgelés. Un plat surgelé acheté, un plat surgelé offert. Une femme divorcée décide de se noyer après avoir découvert cette action sur le prix des plats surgelés.

\p Un piquet de grève est tenu depuis une semaine devant un entrepôt désaffecté.

\p L’hiver venu, il arrive que les villes soient privées d’aération. Ce phénomène est la faute de clochards qui s’étendent sur les bouches d’aération des villes, l’hiver venu.

\p Une mannequin a envoyé en un même instant son assiette de déjeuner à deux virgule six millions de personnes inconnues.

\p Lorsqu’un mendiant est secoué par les forces de l’ordre, il est possible d’ouïr le tintement de l’argent. Les pièces font beaucoup plus de bruit que les billets.

\p Un actuaire part aux sports d’hiver avec un cercueil sur son toit.

\p Un maître bâille. Il attend que son chien défèque pour retourner dormir.

\p Un abattoir fonctionne jusqu’à épuisement des silhouettes qui s’y risquent. La viande morte y est toujours fraîche.

\p Un chevalement ressemble étrangement à un mirador.

\p Un mineur se demande si ses camarades ensevelis après un coup de grisou vont devenir à leur tour une matière fossile exploitable. Il se questionne sur la valeur marchande du corps transformé de ses camarades ensevelis après un coup de grisou.

\p Une entreprise a virtualisé son commerce de proximité.

\p Quelques tombes alignées à la périphérie d’une ville de province. Au-dessus des tombes, des bouquets flétris et le bruit des trains qui passent juste à côté des tombes alignées à la périphérie d’une ville de province.

\p La courbure des écrans mobiles s’adapte bien au visionnage de l’actualité sur les réseaux sociaux.

\p Des magasins sans caissiers ont ouvert leurs portes. La nourriture sous cellophane semble avoir la même apparence, mais la nourriture sous cellophane peut être payée plus rapidement.

\p Il y a eu une troisième fusillade ce mois-ci dans un quartier dit difficile du nord de la ville. La pression sur le gouvernement s’intensifie. Le préfet saute. Pas sa cervelle.

\p Des camionneurs dorment dans leur camion sur une aire d’autoroute. Personne ne se félicite suffisamment de la productivité des camionneurs qui dorment sur leur lieu de travail.

\p Un ouvrier se sentant trahi par le parti ouvrier décide de ne plus voter pour le parti ouvrier. Il vote pour le parti d’extrême droite qui dit qu’il ne trahira pas l’ouvrier. Depuis ce vote, les intellectuels n’ignorent plus l’ouvrier. Ils le méprisent.

\p Des critiques critiquent la télévision lors d’une émission de télévision. Une vieille dame, seule face à sa télévision, répond à sa télévision, en regardant cette émission de télévision où des critiques critiquent la télévision.

\p Une personne sans emploi a renoncé à se présenter à son prochain entretien d’embauche. Il a préféré à la place se rendre sur une voie ferrée. La personne ayant récupéré le corps de la personne sans emploi a démissionné par la suite. Elle ne touchera pas la totalité de ses indemnités.

\p Les locaux du parti des travailleurs ont été construits par un célèbre architecte américain. Ils se situent dans un quartier en vogue de la capitale.

\p Des adolescents sont morts dans une zone périurbaine. Ils ont mis le feu à une voiture dans laquelle ils se trouvaient.

\p Sur le bord de la route, les fleurs attachées au panneau de signalisation se sont fanées. Leur emballage plastique semble intact. Les voitures passent.

\p Un individu n’a pas eu de conversation de plus d’une minute avec un autre individu depuis plus d’une année.

\p Il n’y a pas de terre sous l’étiquette. Carottes biologiques : orange électrique.

\p Un délinquant a le même passeport que le juge qui le juge, mais a un nom qui rime différemment que le nom du juge qui le juge, et qui le juge différemment du nom qui rime comme le sien, celui du juge qui juge les noms et nomme les actes.

\p Un vieil homme a été retrouvé mort dans son lit, après une fuite de gaz dans l’immeuble où il vivait. La cause de sa mort n’a pas encore été déterminée.

\p Une tôlière diabétique a reçu de la part de sa direction une boîte de chocolats lors de son départ à la retraite.

\p Un paysan est décédé d’une hémorragie en raison d’un désert médical.

\p L’épicier du coin a fermé boutique. La ville de moins de trois mille habitants n’a désormais plus de coin.

\p Un éleveur a tué toutes ses bêtes avant de retourner son arme contre lui. L’éleveur ne sera pas enterré avec ses bêtes.

\p Les moissonneuses-batteuses sont autoguidées par satellite. Le blé est récolté efficacement. Personne n’a plus à se fatiguer. Le prix du pain quotidien ne change pas. L’agriculture progresse. La société aussi.

\p Le croissance économique n’arrive pas à concurrencer les chiffres du chômage.

\p Ils n’ont plus de voix, car ils n’ont plus de dents.

\p Un consommateur passe sa soirée à comparer les prix de téléphones intelligents. À la fin de la soirée, il n’a rien consommé. Le consommateur n’a pas assez d’argent pour consommer le prix d’un téléphone intelligent.

\p Des journalistes se moquent de la chemise d’un représentant ouvrier. Des journalistes blâment un ouvrier d’avoir arraché la chemise de son patron. Des journalistes évoquent une chemise où se trouve un plan social pour ouvriers.

\p Un salaire se mérite comme un bon repas.

\p Un fonctionnaire du service de l’immigration se désinfecte les mains après chaque rendez-vous. Après la fin de tous les rendez-vous et après avoir regagné son appartement, il ouvre la cage de sa perruche, l’embrasse, puis la remet dans sa cage.

\p Une femme au foyer est morte cette nuit des suites d’un traumatisme crânien. Elle avait pourtant fini de faire la vaisselle.

\p Un ouvrier agricole a été mis en jachère par erreur. Le plan d’urgence pour l’emploi n’a rien pu faire.

\p Des machinistes travaillant au grand air font la mine. La chose est dangereuse, ils risquent de s’évaporer.

\p Les premières gelées recouvrent les bancs. Aux premières heures du jour, les bancs se secouent pour se réchauffer.

\p Les immeubles modernistes ont été imaginés pour les classes ouvrières. Ils sont aujourd’hui meublés avec goût par les classes non ouvrières.

\p La restauration rapide s’affaire à distribuer des ventres hachés.

\p Des réfugiés se réfugient dans un quartier pauvre. Sans domicile, ils avaient néanmoins le choix.

\p Au buffet de la gare, la province n’attend personne face à sa bière.

\p Toucher le salaire minimal est une bonne raison de se jeter par la fenêtre pour toucher un peu plus.

\p Des agriculteurs sont reçus par l’exécutif, qui leur promet de tenir la promesse de ne pas les exécuter.

\p Le congé rémunéré brisera bientôt la grève.

\p Quand la foule se divise, il ne reste que des déchets.

\p Un clandestin a été retrouvé dans la cave d’une habitation à loyer modéré grâce à son odeur. Ses restes demeuraient sans-papiers.

\p Le plat du jour à la cantine d’une usine prochainement fermée : nouilles sautées aux légumes.

\p Tout travailleur à la chaîne doit bien servir l’entreprise à laquelle il est attaché.

\p Le progrès est revenu. Nous pouvons nous reposer.
